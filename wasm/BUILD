# Description:
#   TensorFlow Lite minimal inference tool.

load("@rules_cc//cc:defs.bzl", "cc_binary")
load("@emsdk//emscripten_toolchain:wasm_rules.bzl", "wasm_cc_binary")


BASE_LINKOPTS = [
    "-s EXPORTED_FUNCTIONS='[\'_malloc\', \'_free\']'",
    "-s EXPORTED_RUNTIME_METHODS='[\'cwrap\', \'getValue\']'",
    "-s ALLOW_MEMORY_GROWTH=1",
    "-s USE_PTHREADS=0",
    "-s MODULARIZE=1", 
    "-O3",
    "-g3"
]

SRC_FILES = [
  "img_tracker_wasm.cc",
  "img_tracker_wasm.h",
  "utils.h",
  "utils.cc"
  ]


g2o_SRC = [
  #types
  "g2o/types/types_sba.h",
  "g2o/types/types_six_dof_expmap.h",
  "g2o/types/types_sba.cpp",
  "g2o/types/types_six_dof_expmap.cpp",
  "g2o/types/types_seven_dof_expmap.cpp",
  "g2o/types/types_seven_dof_expmap.h",
  "g2o/types/se3quat.h",
  "g2o/types/se3_ops.h",
  "g2o/types/se3_ops.hpp",
  #core
  "g2o/core/base_edge.h",
  "g2o/core/base_binary_edge.h",
  "g2o/core/hyper_graph_action.cpp",
  "g2o/core/base_binary_edge.hpp",
  "g2o/core/hyper_graph_action.h",
  "g2o/core/base_multi_edge.h",
  "g2o/core/hyper_graph.cpp",
  "g2o/core/base_multi_edge.hpp" ,        
  "g2o/core/hyper_graph.h",
  "g2o/core/base_unary_edge.h" ,         
  "g2o/core/linear_solver.h",
  "g2o/core/base_unary_edge.hpp" ,        
  "g2o/core/marginal_covariance_cholesky.cpp",
  "g2o/core/base_vertex.h"               ,
  "g2o/core/marginal_covariance_cholesky.h",
  "g2o/core/base_vertex.hpp"             ,
  "g2o/core/matrix_structure.cpp",
  "g2o/core/batch_stats.cpp"       ,
  "g2o/core/matrix_structure.h",
  "g2o/core/batch_stats.h",       
  "g2o/core/openmp_mutex.h",
  "g2o/core/block_solver.h",             
  "g2o/core/block_solver.hpp",            
  "g2o/core/parameter.cpp",           
  "g2o/core/parameter.h",             
  "g2o/core/cache.cpp",              
  "g2o/core/cache.h",
  "g2o/core/optimizable_graph.cpp",       
  "g2o/core/optimizable_graph.h",   
  "g2o/core/solver.cpp",        
  "g2o/core/solver.h",
  "g2o/core/creators.h",                 
  "g2o/core/optimization_algorithm_factory.cpp",
  "g2o/core/estimate_propagator.cpp",    
  "g2o/core/optimization_algorithm_factory.h",
  "g2o/core/estimate_propagator.h",
  "g2o/core/factory.cpp",      
  "g2o/core/optimization_algorithm_property.h",
  "g2o/core/factory.h",
  "g2o/core/sparse_block_matrix.h",
  "g2o/core/sparse_optimizer.cpp",
  "g2o/core/sparse_block_matrix.hpp",
  "g2o/core/sparse_optimizer.h",
  "g2o/core/hyper_dijkstra.cpp", 
  "g2o/core/hyper_dijkstra.h",
  "g2o/core/parameter_container.cpp",    
  "g2o/core/parameter_container.h",
  "g2o/core/optimization_algorithm.cpp",
  "g2o/core/optimization_algorithm.h",
  "g2o/core/optimization_algorithm_with_hessian.cpp",
  "g2o/core/optimization_algorithm_with_hessian.h",
  "g2o/core/optimization_algorithm_levenberg.cpp", 
  "g2o/core/optimization_algorithm_levenberg.h",
  "g2o/core/optimization_algorithm_gauss_newton.cpp",
  "g2o/core/optimization_algorithm_gauss_newton.h",
  "g2o/core/jacobian_workspace.cpp",
  "g2o/core/jacobian_workspace.h",
  "g2o/core/robust_kernel.cpp",
  "g2o/core/robust_kernel.h",
  "g2o/core/robust_kernel_factory.cpp",
  "g2o/core/robust_kernel_factory.h",
  "g2o/core/robust_kernel_impl.cpp",
  "g2o/core/robust_kernel_impl.h",
  #stuff
  "g2o/stuff/string_tools.h",
  "g2o/stuff/color_macros.h",
  "g2o/stuff/macros.h",
  "g2o/stuff/timeutil.cpp",
  "g2o/stuff/misc.h",
  "g2o/stuff/timeutil.h",
  "g2o/stuff/os_specific.c",
  "g2o/stuff/os_specific.h",
  "g2o/stuff/string_tools.cpp",
  "g2o/stuff/property.cpp",  
  "g2o/stuff/property.h"
]
DBoW2_SRC = [
  "DBoW2/DBoW2/BowVector.h",
  "DBoW2/DBoW2/FORB.h",
  "DBoW2/DBoW2/FClass.h",       
  "DBoW2/DBoW2/FeatureVector.h",
  "DBoW2/DBoW2/ScoringObject.h", 
  "DBoW2/DBoW2/TemplatedVocabulary.h",
  "DBoW2/DBoW2/BowVector.cpp",
  "DBoW2/DBoW2/FORB.cpp",      
  "DBoW2/DBoW2/FeatureVector.cpp",
  "DBoW2/DBoW2/ScoringObject.cpp",
  "DBoW2/DUtils/Random.h",
  "DBoW2/DUtils/Timestamp.h",
  "DBoW2/DUtils/Random.cpp",
  "DBoW2/DUtils/Timestamp.cpp"
]

SLAM_SRC =[]



cc_binary(
  name = "tflite",
  srcs = SRC_FILES,
  linkopts = BASE_LINKOPTS + [
        "-s EXPORT_NAME=ImageTrackerModule",
  ],
  deps = [
    "@opencv//:opencv"
  ],
)
wasm_cc_binary(
    name = "tflite-wasm",
    cc_target = ":tflite",
)

cc_binary(
  name = "tflite-simd",
  srcs = SRC_FILES,
  linkopts = BASE_LINKOPTS +  [
    "-s EXPORT_NAME=ImageTrackerSIMDModule"

  ],
  deps = [
    "@opencv//:opencv_simd",
  ],
)
wasm_cc_binary(
    name = "tflite-simd-wasm",
    cc_target = ":tflite-simd",
    simd = True,
)
cc_binary(
  name = "tflite-threaded-simd",
  srcs = SRC_FILES,
  linkopts = BASE_LINKOPTS +  [
    "-s EXPORT_NAME=ImageTrackerSIMDThreadedModule",
    # "-s DEMANGLE_SUPPORT=0",
    # "-s USE_PTHREADS=1",
    # "-s PROXY_TO_PTHREAD=1"
  ],
  deps = [
    "@opencv//:opencv_simd",
  ],
)
wasm_cc_binary(
    name = "tflite-wasm-threaded-simd",
    cc_target = ":tflite-threaded-simd",
    simd = True,
)