const N = 10;

export class ImageTracker {
    constructor(width, height, callback) {
        let _this = this;

        this._width = width;
        this._height = height;

        this.valid = false;

        const WASM_HAS_SIMD_SUPPORT=  WebAssembly.validate(new Uint8Array([
            0, 97, 115, 109, 1, 0, 0, 0, 1,  4, 1,   96, 0,  0, 3,
            2, 1,  0,   10,  9, 1, 7, 0, 65, 0, 253, 15, 26, 11
          ]));
    
    
    
        const WASM_HAS_MULTITHREAD_SUPPORT= WebAssembly.validate(new Uint8Array([
            0, 97, 115, 109, 1, 0,  0,  0, 1, 4, 1,  96, 0,   0,  3, 2, 1,  0, 5,
            4, 1,  3,   1,   1, 10, 11, 1, 9, 0, 65, 0,  254, 16, 2, 0, 26, 11
          ]));
    
          if (!WASM_HAS_SIMD_SUPPORT) {
            //wasmBinaryFile = 'tflite/tflite.wasm';
            ImageTrackerModule().then((Module)=> {
                console.log("WASM module loaded.");
                this.onWasmInit(Module);
                if (callback) callback();
            });
        }else{
            if (WASM_HAS_MULTITHREAD_SUPPORT) {
                ImageTrackerSIMDThreadedModule().then((Module)=> {
                    console.log("WASM SIMD Threaded module loaded.");
                    this.onWasmInit(Module);
                    if (callback) callback();
                });
            } else {
                ImageTrackerSIMDModule().then((Module)=> {
                    console.log("WASM SIMD  module loaded.");
                    this.onWasmInit(Module);
                    if (callback) callback();
                });
            }
        }
    }
 
    onWasmInit(Module) {
        this._Module = Module;

        this._init = this._Module.cwrap("initAR", "number", ["number", "number", "number"]);
        this._resetTracking = this._Module.cwrap("resetTracking", "number", ["number", "number", "number"]);
        this._track = this._Module.cwrap("track", "number", ["number", "number", "number"]);

        this.imPtr = this._Module._malloc(this._width * this._height);
    }

    addRefIm(refIm, refImWidth, refImHeight) {
        return new Promise((resolve, reject) => {
            this.refImPtr = this._Module._malloc(refIm.length);
            this._Module.HEAPU8.set(refIm, this.refImPtr);
            this._init(this.refImPtr, refImWidth, refImHeight);
            resolve();
        });
    }

    parseResult(ptr) {
        const valid = this._Module.getValue(ptr, "i8");
        const dataPtr = this._Module.getValue(ptr + 4, "*");
        let data = new Float64Array(this._Module.HEAPF64.buffer, dataPtr, 17);

        const h = data.slice(0, 9);
        const warped = data.slice(9, 17);

        return {
            valid: valid,
            H: h,
            corners: warped
        };
    }

    resetTracking(im) {
        this._Module.HEAPU8.set(im, this.imPtr);
        const res = this._resetTracking(this.imPtr, this._width, this._height);

        const resObj = this.parseResult(res);
        this.valid = resObj.valid;
        return resObj;
    }

    track(im) {
        // reset tracking if homography is no long valid
        if (!this.valid) {
            return this.resetTracking(im, this._width, this._height);
        }
        this._Module.HEAPU8.set(im, this.imPtr);
        const res = this._track(this.imPtr, this._width, this._height);

        const resObj = this.parseResult(res);
        this.valid = resObj.valid;
        return resObj;
    }
}
